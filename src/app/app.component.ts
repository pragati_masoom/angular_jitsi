import { Component, OnInit } from '@angular/core';
declare global {
  interface Window { exports }
}

@Component({
  selector: 'app-root',   
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit { 
  title = 'angular-Jitsi';
  jitsi: any;
  options = { roomName: 'JitsiTestRoom' };

  constructor() { }

  loading = true;

  ngOnInit() {
    
    window.exports = window.exports || {};
  }
  ngAfterViewInit(){
      // verify the JitsiMeetExternalAPI constructor is added to the global..
      if (window.exports.JitsiMeetExternalAPI) 
      {
        this.startConference();
      } 
    else {alert('Jitsi Meet API script not loaded');}
    }
  

  startConference() {
    try {
     let domain = 'meet.jit.si';
     let options = {
      roomName: 'roomName',
      height: '100%',
      parentNode: document.getElementById('jitsi-container'),
      interfaceConfigOverwrite: {
       filmStripOnly: false,
       SHOW_JITSI_WATERMARK: false,
      },
      configOverwrite: {
       disableSimulcast: false,
      },
     };


     
     let api = new window.exports.JitsiMeetExternalAPI(domain, options);
     api.addEventListener('videoConferenceJoined', () => {
      console.log('Local User Joined');
      this.loading = false;
      api.executeCommand('displayName', 'MyName');
     });
    } catch (error) {
     console.error('Failed to load Jitsi API', error);
    }
   }

}
